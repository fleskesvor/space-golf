# Space Golf

Space Golf was created for the [Linux Game Jam 2019](https://itch.io/jam/linux-game-jam-2019), and is a game about golfing in space.

The game can be downloaded from [itch.io](https://flesk.itch.io/space-golf), or from [GitLab](https://gitlab.com/fleskesvor/space-golf),
where the source code and assets are also hosted.

### Credits

- [fleskesvor](https://gitlab.com/fleskesvor) - Concept, placeholder art and programming

### Controls

- Walk clockwise - right arrow
- Walk counter-clockwise - left arrow
- Jump/fly - space bar, hold for flight
- Hit ball - press and hold x button (when in range), release to hit
- Adjust angle - left/right arrow while holding x to adjust aim

### Software

The game was developed using a variety of free and open-source source software:

- [GNU/Linux](https://www.linuxfoundation.org/) - Operating system
- [Godot Engine](https://godotengine.org/) - Game engine
- [GitLab](https://gitlab.com) - Version control and CI/CD
- [Butler](https://github.com/itchio/butler) - Upload tool for itch.io
- [Inkscape](https://inkscape.org/) - Vector graphics editor
- [GIMP](https://www.gimp.org/) - Image manipulation program
