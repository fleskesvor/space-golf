extends ColorRect

func _ready():
	$"Container/Restart".grab_focus()

func _on_Restart_pressed():
	Global.play_next_scene(true)

func _on_Quit_pressed():
	Global.quit_game()
