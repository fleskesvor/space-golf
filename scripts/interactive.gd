extends KinematicBody2D

const DOWN = Vector2(0, 1)

func _ready():
	add_to_group("interactive")

func pause(pause=true):
	set_physics_process(not pause)

func calculate_rotation():
	var closest_planet
	var shortest_distance

	for planet in get_tree().get_nodes_in_group("planets"):
		var distance = global_position.distance_to(planet.global_position)
		if not shortest_distance or distance < shortest_distance:
			closest_planet = planet
			shortest_distance = distance

	var gravity_vector = closest_planet.global_position - global_position
	return DOWN.angle_to(gravity_vector.normalized())
