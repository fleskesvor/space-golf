extends "interactive.gd"

export (int) var run_speed = 15_000
export (int) var jump_force = 70_000
export (int) var gravity = 10_000
export (int) var friction = 400

var velocity = Vector2()
var speed = Vector2()

func _physics_process(delta):
	var player_rotation = calculate_rotation()

	apply_movement(delta)
	# apply gravity
	speed.y += gravity

	velocity = speed * delta
	velocity = velocity.rotated(player_rotation)

	move_and_slide(velocity)
	self.rotation = player_rotation

func apply_movement(delta):
	if Input.is_action_pressed("ui_left"):
		speed.x = -run_speed
	elif Input.is_action_pressed("ui_right"):
		speed.x = run_speed
	# stop when below friction threshold
	elif abs(speed.x) < friction:
		speed.x = 0
	# slow down when not actively moving
	else:
		speed.x -= friction * speed.x / abs(speed.x)

	# TODO: Disable jump when not on floor - use rockets instead
	if Input.is_action_pressed("ui_accept"):
		speed.y = -jump_force
	else:
		speed.y = jump_force * delta
