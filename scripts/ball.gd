extends "interactive.gd"

export (Vector2) var hit_force = Vector2(100_000, 300_000)
export (int) var gravity = 10_000
export (int) var friction = 5_000

const ROTATION_OFFSET = 5

onready var player = $"../Player"

var velocity = Vector2()
var speed = Vector2()
var facing_left = false
var charge_direction = 1
var last_rotation = 0

func _physics_process(delta):
	var player_rotation = calculate_rotation()
	apply_movement(delta)

	# Workaround to smooth transition between planets
	if last_rotation - player_rotation > PI / 2:
		speed.x = 0
		speed.y += 100_000
	last_rotation = player_rotation

	# apply gravity
	speed.y += gravity

	velocity = speed * delta
	velocity = velocity.rotated(player_rotation)

	move_and_slide(velocity)
	self.rotation = player_rotation

func apply_movement(delta):
	var player_position = player.global_position
	var in_range = global_position.distance_to(player_position) < 50

	if in_range:
		var rotated_player_position = player.calculate_rotation() + ROTATION_OFFSET
		var rotated_ball_position = calculate_rotation() + ROTATION_OFFSET
		var facing_left = rotated_player_position > rotated_ball_position

		if Input.is_action_just_pressed("ui_action"):
			$Arrow.rotation = PI if facing_left else 0
			$Arrow.visible = true
			player.pause()
			speed.x = 0

		if Input.is_action_pressed("ui_action"):
			var arrow_rotation = PI if facing_left else 0
			if $Arrow.scale.x <= 1:
				charge_direction = 1
			elif $Arrow.scale.x >= 3:
				charge_direction = -1
			$Arrow.scale.x += charge_direction * delta
			if Input.is_action_pressed("ui_left") and $Arrow.rotation > arrow_rotation - 0.5:
				$Arrow.rotate(-delta)
			if Input.is_action_pressed("ui_right") and $Arrow.rotation < arrow_rotation + 0.5:
				$Arrow.rotate(delta)

		if Input.is_action_just_released("ui_action"):
			$Arrow.visible = false
			$Arrow.scale.x = 1
			player.pause(false)

			var thrust = $Arrow.scale.x * -1 if facing_left else 1
			var lift = PI - $Arrow.rotation if facing_left else $Arrow.rotation
			speed = hit_force * Vector2(thrust, lift)

	elif abs(speed.x) < friction:
		speed.x = 0
	# slow down when not actively moving
	elif is_on_planet:
		speed.x -= friction * speed.x / abs(speed.x)

var is_on_planet = false

# Workaround for is_on_floor
func _on_Area2D_body_entered(body):
	if body.is_in_group("planets"):
		is_on_planet = true
	if body.is_in_group("interactive"):
		speed.x = 0

func _on_Area2D_body_exited(body):
	if body.is_in_group("planets"):
		is_on_planet = false
