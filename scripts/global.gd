extends Node

var level1 = preload("res://level1.tscn")
var level2 = preload("res://level2.tscn")
var win_scene = preload("res://win_scene.tscn")

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		restart_current_scene()

func play_next_scene(restart=false):
	if restart:
		get_tree().change_scene_to(level1)
	elif get_tree().current_scene.name == "Level1":
		get_tree().change_scene_to(level2)
	else:
		show_win_scene()

func show_win_scene(display=true):
	if display:
		$"/root".add_child(win_scene.instance())
		get_tree().call_group("interactive", "pause")
	else:
		$"/root".remove_child(win_scene)
		get_tree().call_group("interactive", "pause", false)

func restart_current_scene():
	get_tree().reload_current_scene()

func quit_game():
	get_tree().quit()